<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if (isset($container) and $container == 'container-wide') {
    echo 'no-gutters';
} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
    <div class="col-12">
			<?php  
				$contact_form = get_sub_field("contact_form");
				$map = get_sub_field("map");
				$contact_info = get_sub_field("contact_info");
				$address = get_sub_field("address");
				$hours = get_sub_field("hours");
			?>
			<div class="contact-wrapper d-flex flex-wrap">
				<div class="contact-left mb-3 col-md-6">
					<div class="contact-form">
						<?php echo $contact_form; ?>
					</div>
				</div>
				<div class="contact-right col-md-6">
					<div class="form-map mb-3">
						<?php echo $map; ?>
					</div>
					<div class="clinic-info d-flex flex-wrap">
						<div class="col-md-6">
							<div class="contact-info mb-2">
								<?php echo $contact_info; ?>
							</div>
							<div class="contact-address mb-2">
								<?php echo $address; ?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="contact-hours">
								<?php echo $hours; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
