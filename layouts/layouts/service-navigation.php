<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
<?php $container = $container ?? '' ?>
	<div class="d-flex flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php  
				$prev_post = get_previous_post();
				$next_post = get_next_post(); 
			?>
				<div class="service-pagination d-flex flex-column align-items-center flex-md-row flex-wrap justify-content-between">
					<?php if (!empty($prev_post)): ?>
						<a class="py-1" href="<?php echo get_permalink($prev_post->ID) ?>"><i class="fas fa-chevron-left"></i>&nbsp; <?php echo $prev_post->post_title; ?></a>
					<?php endif ?>	
					<?php if (!empty($next_post)): ?>
						<a class="py-1" href="<?php echo get_permalink($next_post->ID) ?>"><?php echo $next_post->post_title; ?> &nbsp;<i class="fas fa-chevron-right"></i></a>
					<?php endif ?>
				</div>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
