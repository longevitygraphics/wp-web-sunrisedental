<div class="top-banner <?php if (is_front_page()) {
    echo 'home-top-banner';
} ?>">
    <?php
    if (is_front_page()): ?>

        <?php
        $top_banner_video_id = get_field("top_banner_video_id");
        $top_banner_form = get_field("top_banner_form");
        $text_overlay = get_field("top_banner_text");
        $mobile_top_banner = get_field("mobile_top_banner");
        $top_banner_video_fallback_image = get_field("top_banner_video_fallback_image");
        $top_banner_video_cta = get_field("top_banner_video_cta");
        ?>

        <div class="top-banner-video" data-cover-image="">
            <div id="myPlayer" style="background: #fff; width: 100%; position: relative; ">

                <div class="container">
                    <div class="top-banner-video-content">
                        <div class="text">
                            <?php echo $text_overlay; ?>
                        </div>
                        <div class="top-banner-video-form d-none d-md-block">
                            <?php echo do_shortcode($top_banner_form); ?>
                        </div>
                        <div class="btn-wrap d-md-none">
                            <a href="<?php echo $top_banner_video_cta['url']; ?>"
                               class="btn btn-primary btn-open-modal"><?php echo $top_banner_video_cta['title']; ?></a>
                        </div>
                    </div>
                </div>

            </div>


            <div id="customElement" class="player"
                 data-property="{videoURL:'<?php echo $top_banner_video_id; ?>',
                 loop:true,
                 containment:'#myPlayer',
                 showControls:false,
                 autoPlay:true,
                 loop:false,
                 mute:true,
                 startAt:0,
                 opacity:1,
                 addRaster:true,
                 quality:'default',
                 coverImage: '<?php echo $top_banner_video_fallback_image['url'] ?>',
                 mobileFallbackImage: '<?php if( is_array($mobile_top_banner) ) echo $mobile_top_banner['url'] ?>',
                 useOnMobile:false,

                 }">

            </div>
        </div>
        <!-- <div class="top-banner-video">
            <div class="video-background d-none d-lg-block">
                <div class="video-foreground">
                   <?php /*echo $top_banner_video; */ ?>
                </div>
            </div>

            <div class="container">
                <div class="top-banner-video-content">
                    <div class="mobile d-lg-none">
                        <div class="embed-responsive embed-responsive-16by9">
                            <?php /*echo $top_banner_video; */ ?>
                        </div>
                    </div>
                    <div class="desktop d-none d-lg-flex">
                        <div class="text">
                            <?php /*echo $text_overlay; */ ?>
                        </div>
                        <div class="top-banner-video-form">
                            <?php /*echo do_shortcode($top_banner_form); */ ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>-->


    <?php else: ?>

        <?php
        $top_banner_images = get_field("top_banner")['gallery'];
        $gallery = $top_banner_images;
        $text_overlay = get_field("text_overlay");
        $top_banner_form = get_field("top_banner_form");
        //pr($top_banner_images);
        ?>
        <?php if ($top_banner_images && is_array($top_banner_images["src"])) : ?>
            <?php
            $banner_image;
            foreach ($top_banner_images['src'] as $top_banner_image) {
                $banner_image = $top_banner_image['image']['url'];
            } ?>
            <?php if ($text_overlay || $top_banner_form) : ?>
                <div class="top-banner-overlay" style="background-image: url(<?php echo $banner_image ?>)">
                    <div class="banner-text container d-xl-flex justify-content-between align-items-center">
                        <div>
                            <?php echo $text_overlay ?>
                        </div>
                        <?php if ($top_banner_form): ?>
                            <div class="top-banner-form d-none d-xl-inline-block">
                                <?php echo do_shortcode($top_banner_form); ?>
                            </div>
                            <div class="d-xl-none">
                                <a class="btn btn-primary btn-open-modal" href="#">Book Appointment</a>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif ?>

    <?php endif; ?>


</div>