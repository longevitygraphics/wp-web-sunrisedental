<?php

function main_nav_items ( $items, $args ) {
    if ($args->menu and is_object($args->menu) and $args->menu->slug == 'top-nav') {
    	$items .= '<li class="align-self-lg-center nav-search"><a href="#" class="nav-link"><i class="fas fa-search"></i></a><form action="/" id="header-search-form" method="get"><input type="text" name="s" id="s" placeholder="Search"><button type="submit"><i class="fas fa-search"></i></button></form></li>';
        $items .= '<li class="nav-book menu-item menu-item-type-custom menu-item-object-custom nav-item d-none d-lg-inline-block"><a class="nav-link btn btn-primary text-white" href="#">Book Appointment</a></li>';
    }
    return $items;
}

add_filter( 'wp_nav_menu_items', 'main_nav_items', 10, 2 );

add_action('wp_content_top', 'featured_banner_top', 1); // ('wp_content_top', defined function name, order)

function featured_banner_top()
{
    ob_start();
    if(!is_search()){
        get_template_part('/templates/template-parts/page/top-banner');
    }
    
    echo ob_get_clean();
}

function example_ajax_request() {
 
    if ( isset($_REQUEST) ) {
        $fruit = $_REQUEST['fruit'];

        if ( $fruit == 'Banana' ) {
            $fruit = do_shortcode('[instagram-feed]');
        }
        echo $fruit;
    }
   die();
}
 
add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );

add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );

function example_ajax_enqueue() {
    wp_localize_script(
        'lg-script-child',
        'example_ajax_obj',
        array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) )
    );
}
add_action( 'wp_enqueue_scripts', 'example_ajax_enqueue' );

function pr($d){
	echo '<pre>';
	print_r($d);
	echo '</pre>';
}