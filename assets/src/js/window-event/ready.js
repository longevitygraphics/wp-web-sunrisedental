// Windows Ready Handler

(function ($) {
  $(document).ready(function () {
    //parent menu item clicakble
    $(".navbar #main-navbar .dropdown > a").click(function () {
      location.href = this.href;
    });
    $(".navbar #main-navbar .dropdown > .dropdown-toggle").after(
      '<i class="fas fa-caret-down"></i>'
    );
    $(".navbar #main-navbar .dropdown").click(function () {
      $(this).children(".dropdown-menu").slideToggle();
    });

    $(".nav-search > .nav-link").click(function () {
      $(this).next("#header-search-form").toggleClass("show-search-form");
    });
    var myPlayer;

    if ($.fn.YTPlayer) {
      myPlayer = jQuery(".player").YTPlayer();
    }

    //home page services
    activateServiceLinks();

    //load the service slick slider
    $(".service-slider__inner").slick({
      cssEase: "ease",
      fade: true,
      adaptiveHeight: true,
      infinite: false,
      prevArrow: $(".service-slider__arrows .left"),
      nextArrow: $(".service-slider__arrows .right"),
    });
  });

  function activateServiceLinks() {
    $(".home-service-items li").click(function () {
      // if ($(this).hasClass('active')) {
      //     return false;
      // }
      $(".home-service-items li").removeClass("active");
      $(this).addClass("active");

      var servicenum = $(this).attr("data-num");
      var activeContent = $(
        ".home-service-items-content > div[data-num=" + servicenum + "]"
      );
      console.log(activeContent);
      $(".home-service-items-content > div").hide();
      activeContent.fadeIn();
    });
  }

  /**** open modal *****/
  function openModal() {
    $(".booking-modal").addClass("open-modal");
  }

  function closeModal() {
    $(".booking-modal").removeClass("open-modal");
  }
  $("header .nav-book").click(function () {
    openModal();
  });
  $(".exit-modal").click(function () {
    closeModal();
  });
  $(".btn-open-modal").click(function () {
    openModal();
  });
  $(".cta-block-btn-open-modal .flexible_call-to-action .btn").click(function (
    e
  ) {
    // e.preventDefault();
    openModal();
  });
  /**** end of open modal *****/

  // $(window).on("scroll", function() {
  //     var element = document.querySelector("footer");
  //     var position = element.getBoundingClientRect();

  //     // checking for partial visibility
  //     if (position.top < window.innerHeight && position.bottom >= 0) {
  //         // We'll pass this variable to the PHP function example_ajax_request
  //         var fruit = "Banana";

  //         // This does the ajax request
  //         $.ajax({
  //             url: example_ajax_obj.ajaxurl, // or example_ajax_obj.ajaxurl if using on frontend
  //             data: {
  //                 action: "example_ajax_request",
  //                 fruit: fruit,
  //             },
  //             success: function(data) {
  //                 // This outputs the result of the ajax request
  //                 $(".footer-instagram-ajax").html(data);
  //             },
  //             error: function(errorThrown) {
  //                 console.log(errorThrown);
  //             },
  //         });
  //         $(window).off("scroll");
  //     }
  //});
})(jQuery);
