<?php
/**
 * Main template file
 *
 * This page shows the search result.
 *
 * @package wp-web-sunrisedental
 */

get_header();

?>

<?php if ( have_posts() ) : ?>
	<div class="container py-3 mt-5">
	<?php
	while ( have_posts() ) :
		the_post();
		?>
		<div class="pb-3">
			<header>
				<h2><a class="text-dark" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			</header>
			<div class="article-content">
				<p><?php the_excerpt(); ?></p>
			</div>
			<a class="d-inline-block mt-4 text-start text-secondary" href="<?php the_permalink(); ?>">CONTINUE READING</a>
        </div>
        <?php if (($wp_query->current_post +1) != ($wp_query->post_count)): ?>
			<hr class="lg d-none d-md-block">
		<?php endif; ?>

	<?php endwhile; ?>
	</div>
<?php else: ?>
        <div class='container py-5 text-center'>
			<h2 class='mb-3'>Sorry, no search result for "<?php echo get_search_query(); ?>"</h2>
            <div class='d-flex justify-content-center'>
                <a class='btn btn-primary' href="/">Back to Home</a>
            </div>
		</div>
<?php endif; ?>

<?php get_footer(); ?>
